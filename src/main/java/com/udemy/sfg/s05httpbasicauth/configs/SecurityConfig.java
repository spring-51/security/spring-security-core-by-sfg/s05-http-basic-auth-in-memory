package com.udemy.sfg.s05httpbasicauth.configs;

import org.springframework.cglib.proxy.NoOp;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // requests.antMatchers(..) is used for whitelisting APIs
        // order of ant matcher is important
        // we can place requests.antMatchers(..) after requests.anyRequest()
        // it will give RTE as java.lang.IllegalStateException: Can't configure antMatchers after anyRequest
        http.authorizeRequests((requests) -> {
            requests
                    // this will whitelist api for all Http methods
                    .antMatchers("/whitelist/public", "/").permitAll()

                    // this will whitelist api for ONLY Http GET
                    .antMatchers(HttpMethod.GET, "/whitelist/get").permitAll()

                    // this will whitelist api for ONLY Http GET, syntax of string matches with @RequestMapping
                    // for such syntax of string we use mvcMatchers(..) in place of antMatchers(..)
                    .mvcMatchers(HttpMethod.GET, "/whitelist/{path}").permitAll()
            ;
        });
        http.authorizeRequests((requests) -> {
            ((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)requests.anyRequest()).authenticated();
        });

        http.formLogin();
        http.httpBasic();
    }

    // this has been commented as we are using configure(AuthenticationManagerBuilder) i.e Fluent way
    // to create users
    /*
    @Override
    @Bean
    protected UserDetailsService userDetailsService() {
        UserDetails admin = User.withDefaultPasswordEncoder()
                .username("admin")
                .password("admin")
                .roles("ADMIN")
                .build();

        UserDetails user = User.withDefaultPasswordEncoder()
                .username("inmemuser")
                .password("inmemuserpass")
                .roles("USER")
                .build();
        return new InMemoryUserDetailsManager(admin,user);
    }

     */

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("admin").password("{noop}admin").roles("ADMIN")
                .and()
                .withUser("inmemuser").password("{noop}inmemuserpass").roles("USER")
                ;
    }

    // below bean is commented because
    // we have used Spring expression lang {noop} within
    // configure(AuthenticationManagerBuilder) while defining user credential
    /*
    @Bean
    public PasswordEncoder passwordEncoder(){
        return NoOpPasswordEncoder.getInstance();
    }
     */


}
