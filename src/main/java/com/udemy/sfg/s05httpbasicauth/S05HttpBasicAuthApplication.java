package com.udemy.sfg.s05httpbasicauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class S05HttpBasicAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(S05HttpBasicAuthApplication.class, args);
    }

}
