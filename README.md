# Http Basic Auth Java Configuration

## References
```ref
1. https://docs.spring.io/spring-security/site/docs/current/reference/html5
```

### L27 - Introduction
```
1. When we define below properties in application properties file.
  spring.security.user.name=admin
  spring.security.user.password=${spring.security.user.name}

or  we are letting spring to create default credentials.
Spring internally uses In-Memory Authentication.
```

### L28 - Spring security authentication process/ components
```
1. Different Spring Security components
 -- 1.1. AuthenticationFilter
 -- 1.2. AuthenticationManager
 -- 1.3. AuthenticationProvider
 -- 1.4. UserDetailsService
 -- 1.5. PasswordEncoder
 -- 1.6. SecurityContext
 
 2. refer ./L28-SpringSecAuthProcess.pdf

```

### L29 - UserDetailsService
```
1. As dissussed in L27.
When we define below properties in application properties file.
  spring.security.user.name=admin
  spring.security.user.password=${spring.security.user.name}

or  we are letting spring to create default credentials.
Spring internally uses In-Memory Authentication, as part of authenntication
spring creates bean of type UserDetailsService.

2. We can override default creation of UserDetailsService type bean by creating this bean
- refer com.udemy.sfg.s04httpbasicauth.configs.SecurityConfig -> userDetailsService()

3. When we create bean of UserDetailsService, spring will not read application properties file for creating user credential,
users will be created as per info passed in com.udemy.sfg.s04httpbasicauth.configs.SecurityConfig -> userDetailsService()
- remove user credential specific properties from application properties file
- 
 @Override
    @Bean
    protected UserDetailsService userDetailsService() {
        UserDetails admin = User.withDefaultPasswordEncoder()
                .username("admin")
                .password("admin")
                .roles("ADMIN")
                .build();

        UserDetails user = User.withDefaultPasswordEncoder()
                .username("inmemuser")
                .password("inmemuserpass")
                .roles("USER")
                .build();
        return new InMemoryUserDetailsManager(admin,user);
    }
```

### L30 - In Memory Authentication - Fluent API
```
1. In L29 we studied creating im memory user by creating UserDetailService type bean.
2. Alternatively we can crete in memory user using Fluent API method.
  - be fluent API we mean method chanining.
3. 
Step 1
To create user using fluent way we need to override overloaded configure(AuthenticationManagerBUilder) 
   of WebSecurityConfigurerAdapter
   - refer com.udemy.sfg.s05httpbasicauth.configs.SecurityConfig ->  configure(AuthenticationManagerBuilder) 
   
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("admin").password("admin").roles("ADMIN")
                .and()
                .withUser("inmemuser").password("inmemuserpass").roles("USER")
                ;
    }
    
Step 2 - create  PasswordEncoder bean (if we are not definig it, we will get RTE as java.lang.IllegalArgumentException: There is no PasswordEncoder mapped for the id "null")
@Bean
    public PasswordEncoder passwordEncoder(){
        return NoOpPasswordEncoder.getInstance();
    }
    
Note - we can avoid above 2 step into one if we use Spring Expression lang
     i.e {noop} while defing user in configure(AuthenticationManagerBuilder) as below
     
     @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("admin").password("{noop}admin").roles("ADMIN")
                .and()
                .withUser("inmemuser").password("{noop}inmemuserpass").roles("USER")
                ;
    }
    
    - refer com.udemy.sfg.s05httpbasicauth.configs.SecurityConfig ->  configure(AuthenticationManagerBuilder)
    - here we don't need to create bean of PasswordEncoder type 
4. When we are using fluent api to create user, we dont need to create UserDetailsServie type bean,
as its useless if we create it, spring will ignore it. User credential will be create as per  configure(AuthenticationManagerBuilder)
```